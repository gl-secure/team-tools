#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'

if ARGV.size.positive?
  puts 'Crunches LF-separated numeric input and outputs statistics in JSON.'
  puts ''
  puts 'Usage:'
  puts ''
  puts "  $#{$PROGRAM_NAME} < /path/to/data/file"
  puts "  $#{$PROGRAM_NAME} (read from STDIN; end with CTRL+D)"
  puts ''
  puts 'where input data contains one number per line.'
  puts ''
  exit 0
end

data = $stdin.readlines.map(&:strip).reject(&:empty?).map(&:to_i)

data.sort!

index = lambda do |pct|
  (pct * data.size).floor
end

percentiles = {
  p50: data[index.call 0.5],
  p75: data[index.call 0.75],
  p90: data[index.call 0.9],
  p95: data[index.call 0.95],
  p99: data[index.call 0.99]
}

result = {
  data: data,
  percentiles: percentiles,
  min: data.min,
  max: data.max,
  avg: data.sum / data.size
}

puts JSON.generate(result)
